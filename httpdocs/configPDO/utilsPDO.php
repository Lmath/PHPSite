<?php

/**
 * Fonction ouvrant la connection a la base de données
 */
function pdoConnection()
{
    $dsn = "mysql:dbname=annuaire;host=127.0.0.1:3306";
    $user = 'root';
    $password = 'root';
    return new PDO($dsn, $user, $password);
}

/**
 *
 * Fonction permettant de retourner un array d'indicatif telephonique
 */
function arrayOfIndicatif()
{

    //on recup la conexion
    $pdo = pdoConnection();

    //Requete en base
    $sql = "SELECT * FROM indicatifs_telephonique ORDER BY NomPays ASC";
    $query = $pdo->prepare($sql);

    //On créé l'array
    $tabIndicatif = [];

    if ($query->execute()) {
        //On peuple l'array
            foreach ($query as $row) {
                array_push($tabIndicatif, "+".$row['IndicatifPays']." : ".$row['NomPays']);
            }

    }
    //On ferme la conexion
    $pdo = null;

    return $tabIndicatif;
}

/**
 *
 * Fonction permettant de retourner un array de contact
 */
function arrayOfContact()
{

    //on recup la conexion
    $pdo = pdoConnection();

    //Requete en base
    $sql = "SELECT * FROM contacts";
    $query = $pdo->prepare($sql);

    //On créé l'array
    $tabContact = [];

    if ($query->execute()) {
        //On peuple l'array
            foreach ($query as $row) {
                array_push($tabContact, $row['ID']." : ".$row['Nom']);
            }

    }
    //On ferme la conexion
    $pdo = null;

    return $tabContact;
}

/**
 * Fonction permettant l'affichage en tableau de tous les contacts
 */
function displayAllContacts()
{
    $pdo = pdoConnection();
    $page = (!empty($_GET['page']) ? $_GET['page'] : 1);
    $limite = 10;

     // Partie "Requête"
     $debut = ($page - 1) * $limite;
     /* Ne pas oublier d'adapter notre requête */
     $query = 'SELECT SQL_CALC_FOUND_ROWS * FROM contacts LIMIT :limite OFFSET :debut';
     $query = $pdo->prepare($query);
     $query->bindValue('debut', $debut, PDO::PARAM_INT);
     $query->bindValue('limite', $limite, PDO::PARAM_INT);
     $query->execute();

    /* Ici on récupère le nombre d'éléments total. Comme c'est une requête, il ne
    * faut pas oublier qu'on ne récupère pas directement le nombre.
    * De plus, comme la requête ne contient aucune donnée client pour fonctionner,
    * on peut l'exécuter ainsi directement */
    $resultFoundRows = $pdo->query('SELECT found_rows()');
    /* On doit extraire le nombre du jeu de résultat */
    $nombredElementsTotal = $resultFoundRows->fetchColumn();

    ?>
    <table aria-describedby="Tableau des contacts">
        <thead>
            <tr id="contact">
                <th colspan="2" id="description">Liste des contacts</th>
            </tr>
            <tr id="legend">
                <th id="info">contact</th>
                <th id="supp">supression</th>
            </tr>
        </thead>
        <?php
    // Partie "Boucle"
    while ($row = $query->fetch()) {
        ?>
                <tr>
                    <td id="contact">
                        <a href="./src/pagePerso.php?ID=<?= $row['ID'] ?>"> Nom : <?= $row['Nom'] ?>, Prenom :<?= $row['Prenom'] ?>
                    </td>
                    <td id="supp">
                        <a href="./src/Suppression/suppressionContact.php?ID=<?= $row['ID'] ?>" class="material-icons">delete</a>
                    </td>
                </tr>
            <?php
            }
            ?>
        </table>
    <?php
    // Partie "Liens"
    /* On calcule le nombre de pages */
    $nombreDePages = ceil($nombredElementsTotal / $limite);

    /* Si on est sur la première page, on n'a pas besoin d'afficher de lien
     * vers la précédente. On va donc l'afficher que si on est sur une autre
     * page que la première */
    if ($page > 1):
        ?><a href="?page=<?php echo $page - 1; ?>">Page précédente</a> — <?php
    endif;

    /* On va effectuer une boucle autant de fois que l'on a de pages */
    for ($i = 1; $i <= $nombreDePages; $i++):
        ?><a href="?page=<?php echo $i; ?>"><?php echo $i; ?></a> <?php
    endfor;

    /* Avec le nombre total de pages, on peut aussi masquer le lien
     * vers la page suivante quand on est sur la dernière */
    if ($page < $nombreDePages):
        ?>— <a href="?page=<?php echo $page + 1; ?>">Page suivante</a><?php
    endif;
    
}

/**
 * Affichage des contact avec un filtre sur le code postal
 * 
 * @param codePostal = code postal servant de critere de recherche
 */
function displayAllContactFilterCodePostal($codePostal){
    $pdo = pdoConnection();
    $page = (!empty($_GET['page']) ? $_GET['page'] : 1);
    $limite = 10;

     // Partie "Requête"
     $debut = ($page - 1) * $limite;
     /* Ne pas oublier d'adapter notre requête */
     $query = 'SELECT SQL_CALC_FOUND_ROWS * FROM contacts WHERE Code_Postal = :codePostal LIMIT :limite OFFSET :debut';
     $query = $pdo->prepare($query);
     $query->bindValue('debut', $debut, PDO::PARAM_INT);
     $query->bindValue('limite', $limite, PDO::PARAM_INT);
     $query->bindValue('codePostal', $codePostal);
     $query->execute();

    /* Ici on récupère le nombre d'éléments total. Comme c'est une requête, il ne
    * faut pas oublier qu'on ne récupère pas directement le nombre.
    * De plus, comme la requête ne contient aucune donnée client pour fonctionner,
    * on peut l'exécuter ainsi directement */
    $resultFoundRows = $pdo->query('SELECT found_rows()');
    /* On doit extraire le nombre du jeu de résultat */
    $nombredElementsTotal = $resultFoundRows->fetchColumn();

    ?>
    <table aria-describedby="Tableau des contacts">
        <thead>
            <tr id="contact">
                <th colspan="2" id="description">Liste des contacts</th>
            </tr>
            <tr id="legend">
                <th id="info">contact</th>
                <th id="supp">supression</th>
            </tr>
        </thead>
        <?php
    // Partie "Boucle"
    while ($row = $query->fetch()) {
        ?>
                <tr>
                    <td id="contact">
                        <a href="./src/pagePerso.php?ID=<?= $row['ID'] ?>"> Nom : <?= $row['Nom'] ?>, Prenom :<?= $row['Prenom'] ?>
                    </td>
                    <td id="supp">
                        <a href="./src/Suppression/suppressionContact.php?ID=<?= $row['ID'] ?>" class="material-icons">delete</a>
                    </td>
                </tr>
            <?php
            }
            ?>
        </table>
    <?php
    // Partie "Liens"
    /* On calcule le nombre de pages */
    $nombreDePages = ceil($nombredElementsTotal / $limite);

    /* Si on est sur la première page, on n'a pas besoin d'afficher de lien
     * vers la précédente. On va donc l'afficher que si on est sur une autre
     * page que la première */
    if ($page > 1):
        ?><a href="?page=<?php echo $page - 1; ?>">Page précédente</a> — <?php
    endif;

    /* On va effectuer une boucle autant de fois que l'on a de pages */
    for ($i = 1; $i <= $nombreDePages; $i++):
        ?><a href="?page=<?php echo $i; ?>"><?php echo $i; ?></a> <?php
    endfor;

    /* Avec le nombre total de pages, on peut aussi masquer le lien
     * vers la page suivante quand on est sur la dernière */
    if ($page < $nombreDePages):
        ?>— <a href="?page=<?php echo $page + 1; ?>">Page suivante</a><?php
    endif;
}

/**
 * Function permettant l'insertion en base d'un contact
 * L'id n'est pas geré ici la base de donnée utilise un champs id autoIcrémenté
 *
 * @param nom = nom du contact
 * @param prenom = prenom du contact
 * @param adresse = adresse du contact
 * @param dateNaissance = date de naissance du contact
 */
function addContact($nom, $prenom, $societe, $adresse, $dateNaissance, $codePostal)
{
    //on recup la conexion
    $pdo = pdoConnection();
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    //Requete en base
    $sql = "INSERT INTO contacts (Nom, Prenom, Societe, Adresse_Postal, Date_Naissance, Code_Postal) VALUES (:nom, :prenom, :societe, :adresse, :dateNaissance, :codePostal)";
    $query = $pdo->prepare($sql);

    //Remplissage des champs
    $query->bindParam(':nom', $nom);
    $query->bindParam(':prenom', $prenom);
    $query->bindParam(':societe', $societe);
    $query->bindParam(':adresse', $adresse);
    $query->bindParam(':dateNaissance', $dateNaissance);
    $query->bindParam(':codePostal', $codePostal);

    //Execution de la requete
    $query->execute();

    //on ferma le conexion
    $pdo = null;
}

/**
 * Function permettant la modif en base d'un contact
 * L'id n'est pas geré ici la base de donnée utilise un champs id autoIcrémenté
 *
 * @param ID = id du contact
 * @param nom = nom du contact
 * @param prenom = prenom du contact
 * @param adresse = adresse du contact
 * @param dateNaissance = date de naissance du contact
 */
function modifContact($ID, $nom, $prenom, $societe, $adresse, $dateNaissance, $codePostal){
    //on recup la conexion
    $pdo = pdoConnection();

    //Requete en base
    $sql = "UPDATE contacts SET Nom = :nom, Prenom = :prenom,
                   Societe = :societe, Adresse_Postal = :adresse,
                   Date_Naissance = :dateNaissance, Code_Postal = :codePostal
                   WHERE ID = :ID";
    $query = $pdo->prepare($sql);

    //Remplissage des champs
    $query->bindParam(':ID', $ID);
    $query->bindParam(':nom', $nom);
    $query->bindParam(':prenom', $prenom);
    $query->bindParam(':societe', $societe);
    $query->bindParam(':adresse', $adresse);
    $query->bindParam(':dateNaissance', $dateNaissance);
    $query->bindParam(':codePostal', $codePostal);

    //Execution de la requete
    $query->execute();

    //on ferma le conexion
    $pdo = null;
}

/**
 * Function permettant la modif en base d'un contact
 * L'id n'est pas geré ici la base de donnée utilise un champs id autoIcrémenté
 *
 * @param numeroBase = numero de base
 * @param numero = nouveau numero
 * @param indicatif = indicatif du num
 * @param type = type de num
 * @param contact = contact du num
 */
function modifNumTel($numeroBase ,$numero, $contact, $indicatif, $type){
    //on recup la conexion
    $pdo = pdoConnection();
    //Requete en base
    $sql = "UPDATE num_tel SET Numero = :num, Id_Contact = :idContact,
                   TypeNum = :typeNum, Id_Indicatif = :idIndicatif
                   WHERE Numero = :numeroBase";
    $query = $pdo->prepare($sql);

    //Remplissage des champs
    $query->bindParam(':num', $numero);
    $query->bindParam(':idContact', $contact);
    $query->bindParam(':typeNum', $type);
    $query->bindParam(':idIndicatif', $indicatif);
    $query->bindParam(':numeroBase', $numeroBase);

    //Execution de la requete
    $query->execute();

    //on ferma le conexion
    $pdo = null;
}

/**
 * Function permettant la recupération d'info d'un contact par id
 *
 * @param ID = id du contact
 */
function contactInfo($ID)
{
    //on recup la conexion
    $pdo = pdoConnection();

    //Requete en base
    $sql = "SELECT * FROM contacts WHERE ID = :ID";
    $query = $pdo->prepare($sql);

    //Remplissage des champs
    $query->bindParam(':ID', $ID);

    $return = [
        "nom" => "",
        "prenom" => "",
        "adresse" => "",
        "société" => "",
        "Date_Naissance" => "",
        "codePostal" => ""
    ];
    //Execution de la requete
    $query->execute();
    foreach ($query as $row) {
        $return["nom"] = $row['Nom'];
        $return["prenom"] = $row['Prenom'];
        $return["adresse"] = $row['Adresse_Postal'];
        $return["société"] = $row['Societe'];
        $return["DateNaissance"] = $row['Date_Naissance'];
        $return["codePostal"] = $row['Code_Postal'];
    }
    //on ferma le conexion
    $pdo = null;
    return $return;
}

/**
 * Function permettant la recupération d'info d'un numero par numero
 *
 * @param num = numero de tel
 */
function numeroInfo($num)
{
    //on recup la conexion
    $pdo = pdoConnection();

    //Requete en base
    $sql = "SELECT * FROM num_tel WHERE Numero = :num";
    $query = $pdo->prepare($sql);

    //Remplissage des champs
    $query->bindParam(':num', $num);

    $return = [
        "contact" => "",
        "indicatif" => "",
        "numero" => "",
        "typeNumero" => ""
    ];
    //Execution de la requete
    $query->execute();
    foreach ($query as $row) {
        $return["contact"] = $row['Id_Contact'];
        $return["indicatif"] = $row['Id_Indicatif'];
        $return["numero"] = $row['Numero'];
        $return["typeNumero"] = $row['TypeNum'];
    }
    //on ferma le conexion
    $pdo = null;
    return $return;
}

/**
 *
 * Fonction permettant l'affichage en tableau de tous les numeros d'un contacts
 * @param idContact = id du contacts au quel on veux les numeros
 */
function displayAllNumsTel($idContact)
{

    //on recup la conexion
    $pdo = pdoConnection();

    //Requete en base
    $sql = "SELECT * FROM num_tel
        Where Id_Contact = :idContact";
    $query = $pdo->prepare($sql);

    //Exuction de la requete avec comme param idContact dans la requete le parametre idContact
    if ($query->execute(['idContact' => $idContact])) {
        //On créé la tableau
?>
        <table aria-describedby="Tableau des numeros d'un contact">
            <thead>
                <tr id="numero">
                    <th colspan="4" id="description">Liste des numeros</th>
                </tr>
                <tr id="legend">
                    <th id="indicatif">indicatif</th>
                    <th id="num">num</th>
                    <th id="modification">modification</th>
                    <th id="supression">suppression</th>
                </tr>
            </thead>
            <?php
            //on le remplis avec les colones de la base <a href="./src/pagePerso.php?ID=
            foreach ($query as $row) {
            ?>
                <tr>
                    <td id="indicatif">
                        <p><?= $row['Id_Indicatif'] ?> </p>
                    </td>
                    <td id="numero">
                        <p><?= $row['Numero'] ?> </p>
                    </td>
                    <td id="modification">
                        <a href="numeroModification?numero=<?= $row['Numero'] ?>" class="material-icons">create</a>
                    </td>
                    <td id="supp">
                        <a href="./Suppression/suppressionNumTel.php?numero=<?= $row['Numero'] ?>" class="material-icons">delete</a>
                    </td>
                </tr>
            <?php
            }
            ?>
        </table>
<?php
    }
    //On ferme la conexion
    $pdo = null;
}

/**
 * 
 * On supprime un contact
 * 
 * @param ID = id du contact a supprimé
 */
function suppressionContact($ID){
    //on recup la conexion
    $pdo = pdoConnection();

    //Requete en base
    $sql = "DELETE  FROM contacts WHERE ID = :id";
    $query = $pdo->prepare($sql);

    //Remplissage des champs
    $query->bindParam(':id', $ID);


    //Execution de la requete
    $query->execute();
    //on ferma le conexion
    $pdo = null;
}
/**
 * 
 * On supprime un contact
 * 
 * @param ID = id du contact a supprimé
 */
function suppressionNumTel($numero){
    //on recup la conexion
    $pdo = pdoConnection();

    //Requete en base
    $sql = "DELETE FROM num_tel WHERE Numero = :numero";
    $query = $pdo->prepare($sql);

    //Remplissage des champs
    $query->bindParam(':numero', $numero);


    //Execution de la requete
    $query->execute();
    //on ferma le conexion
    $pdo = null;
}

/**
 * Function permettant l'insertion en base d'un numéro de teléphone
 * L'id n'est pas geré ici la base de donnée utilise un champs id autoIcrémenté
 *
 * @param numero = numero
 * @param idContact = id du contact
 * @param indicatif = indicatif
 * @param TypeNum = type de num
 */
function addNumTel($numero, $idContact, $indicatif, $TypeNum)
{
    //on recup la conexion
    $pdo = pdoConnection();
    //Requete en base
    $sql = "INSERT INTO num_tel (Numero, Id_Contact, Id_Indicatif, TypeNum) VALUES (:numero, :idContact, :indicatif, :TypeNum)";
    $query = $pdo->prepare($sql);

    //Remplissage des champs
    $query->bindParam(':numero', $numero);
    $query->bindParam(':idContact', $idContact);
    $query->bindParam(':indicatif', $indicatif);
    $query->bindParam(':TypeNum', $TypeNum);

    //Execution de la requete
    $query->execute();

    //on ferma le conexion
    $pdo = null;
}

?>