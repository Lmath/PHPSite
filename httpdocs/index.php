<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="styles/style.css" rel="stylesheet" type="text/css" media="all" />
    <title>Site PHP</title>
</head>

<body class="content">
    <?php
    //Ajout du fichier qui gere les connections avec la bdd
    include 'configPDO/utilsPDO.php';
    include './src/formBuilder.php';
    const CODE_POSTAL = "code Postal";


    //Affichage de la liste des contacts
    displayAllContacts();
    
    ?>
    <br/>
    <a href="src/addContact.php">Ajouter un contact</a>
    <br/>
    <a href="src/addNumTel.php">Ajouter un numero</a>
    <br/>
    <a href="./filtreByCodePostal.php">Filtrer par code</a>
</body>
</html>