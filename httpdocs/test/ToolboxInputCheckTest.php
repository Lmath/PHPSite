<?php

include "../src/ToolboxInputCheck.php";
use PHPUnit\Framework\TestCase;

class ToolboxInputCheckTest extends TestCase
{
    private $formatter;
    private const MANDATORY = "Ce champ est obligatoire";

    protected function setUp()
    {
        $this->formatter = new ToolboxInputCheck;
    }

    public function testStringHaveWellFormedIndicatif()
    {
        $result = $this->formatter->stringHaveWellFormedIndicatif("", $result);
        $this->assertEquals(self::MANDATORY, $result);
    }

    public function testOnlyAlphaAndMandatory()
    {
        $result = $this->formatter->onlyAlphaAndMandatory("", $result);
        $this->assertEquals(self::MANDATORY, $result);
    }

    public function testStrLenghtToLongWithLongString()
    {
        $longStr = "abcdefghijklmnopqrstuvwxyz";
        $result = $this->formatter->strLenghtToLong($longStr, $result, 25);
        $this->assertTrue($result);
    }

    public function testStrLenghtToLongWithShortString()
    {
        $shortStr = "abcde";
        $result = $this->formatter->strLenghtToLong($shortStr, $result, 25);
        $this->assertFalse($result);
    }

    public function testDateIsInPastAndMandatory()
    {
        $result = $this->formatter->dateIsInPastAndMandatory("", $result);
        $this->assertEquals(self::MANDATORY, $result);
    }

    public function testOnlyNumericAndMandatory()
    {
        $result = $this->formatter->onlyNumericAndMandatory("", $result);
        $this->assertEquals(self::MANDATORY, $result);
    }

    public function testDateIsInPastWithDateFutur()
    {
        //Test non passant date dans le futur
        $dateFutur = "9999-12-31";
        $result = $this->formatter->dateIsInPast($dateFutur, $result);
        $this->assertEquals("La date est invalide", $result);
    }

    public function testDateIsInPastWithDateOld()
    {
        //Test passant date dans le passé
        $dateOld = "1000-01-01";
        $result = $this->formatter->dateIsInPast($dateOld, $result);
        $this->assertEquals("", $result);
    }
}
