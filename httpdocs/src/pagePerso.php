<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../styles/style.css" rel="stylesheet" type="text/css" media="all" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <title>Page Perso</title>
</head>

<body class='content'>
    <?php
    include '../configPDO/utilsPDO.php';
    $info = contactInfo($_GET['ID']);
    //On affiche la liste des numéro du contact
    ?>
    <p>Nom : <?php echo $info["nom"]; ?></p>
    <p>Prenom : <?php echo $info["prenom"]; ?></p>
    <p>Adresse : <?php echo $info["adresse"]; ?></p>
    <p>Societé : <?php echo $info["société"]; ?></p>
    <p>Date de naissance : <?php echo $info["Date_Naissance"]; ?></p>
    <p>Code postal : <?php echo $info["codePostal"]; ?></p>
    <?php displayAllNumsTel(htmlspecialchars($_GET['ID']));?>
    <a href="addNumTel?ID=<?= htmlspecialchars($_GET['ID']) ?>">Ajouter un numero</a>
    <br/>
    <a href="contactModification?ID=<?= htmlspecialchars($_GET['ID']) ?>" class="material-icons">create</a>
    <br/>
    <a href="../index.php">Accueil</a>
</body>
<?php require_once('layout/footer.php') ?>
</html>