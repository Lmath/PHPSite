<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../../styles/style.css" rel="stylesheet" type="text/css" media="all" />
    <title>Page Perso</title>
</head>

<body class='content'>
    <?php
    include '../../configPDO/utilsPDO.php';
    include '../formBuilder.php';
    //on recup les info du contact
    const ID = "ID";
    $info = contactInfo($_GET[ID]);

    if (isset($_POST["soumission"])) {
        //On supprime (en cascade donc les numéro associé supprimer aussi) et redirige vers accueil
        suppressionContact($_GET[ID]);
        header("Location: ../../index.php");
    }
    
    //Formulaire de suppresion
    $formAddContact = new FormBuilder("POST", "suppressionContact?ID=".htmlspecialchars($_GET[ID]), "supression d'un contact"); ?>
    <p>Voulez vous vraiment supprimer le contact : <?php echo $info["nom"]." ".$info["prenom"]?> ?</p>
    <?php
    $formAddContact->addSubmit("supprimer");
    $formAddContact->addHiddenSubmit();
    ?>
    <a href="../../index.php"><input type="button" value="Retour"></a>
    <?php
    $formAddContact->finishForm();
    ?>

</body>
</html>