<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../../styles/style.css" rel="stylesheet" type="text/css" media="all" />
    <title>Page Perso</title>
</head>

<body class='content'>
    <?php
    include '../../configPDO/utilsPDO.php';
    include '../formBuilder.php';
    //on recup les info du contact
    const NUMERO = "numero";
    $info = numeroInfo($_GET[NUMERO]);
    if (isset($_POST["soumission"])) {
        //On supprime (en cascade donc les numéro associé supprimer aussi) et redirige vers accueil
        suppressionNumTel($_GET[NUMERO]);
        header("Location: ../../index.php");
    }
    
    //Formulaire de suppresion
    $formAddContact = new FormBuilder("POST", "suppressionNumTel?numero=".htmlspecialchars($_GET[NUMERO]), "supression d'un numero"); 
    $num = $_GET[NUMERO];
    $safeGet = htmlspecialchars($num);
    ?>
    <p>Voulez vous vraiment supprimer le numero : <?php echo $safeGet." de type ".$info["typeNumero"]." avec l'indicatif ".$info["indicatif"]?> ?</p>
    <?php
    $formAddContact->addSubmit("supprimer");
    $formAddContact->addHiddenSubmit();
    ?>
    <a href="../../index.php"><input type="button" value="Retour"></a>
    <?php
    $formAddContact->finishForm();
    ?>
</body>
</html>