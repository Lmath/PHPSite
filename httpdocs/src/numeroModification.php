<?php

/**
 * Formulaire de création d'un numéro de telephone
 */
include 'formBuilder.php';
include 'ToolboxInputCheck.php';
include '../configPDO/utilsPDO.php';
$info = numeroInfo($_GET["numero"]);
//Déclaration des constante de taille et nom des champs
const TAILLENUM = 10;
const TAILLETYPE = 50;
const NUMERO = "numero";
const INDICATIF = "indicatif";
const TYPE = "type";
const CONTACT = "contact";

//Preparation des messages d'erreurs
$erreurs[NUMERO] = "";
$erreurs[INDICATIF] = "";
$erreurs[TYPE] = "";

//Création du formatter pour les différents inputs
$formatter = new ToolboxInputCheck;

//Check des valeurs des champs apres click sur submit
if (isset($_POST["soumission"])) {
    $valeursOK = true;

    /**
     * Champ NUMERO
     * doit etre au format telephone et obligatoire
     */
    $erreurs[NUMERO] = $formatter->onlyNumericAndMandatory($_POST[NUMERO], $valeursOK);
    //Et de taille maximum 10 (on accepte plus petit pour numéros urgence 15/16/112 etc)
    if ($formatter->strLenghtToLong($_POST[NUMERO], $valeursOK, TAILLENUM)) {
        $erreurs[NUMERO] = "le numéro est trop long";
    }

    /**
     * Champ INDICATIF
     * On reçoit au format +000, on doit retiré le + pour l'inserer en base
     */
    $indicatif = str_replace("+","" ,$_POST[INDICATIF]);

    /**Champ TYPE
     * de taille maximun 50
     */
    if ($formatter->strLenghtToLong($_POST[TYPE], $valeursOK, TAILLETYPE)) {
        $erreurs[TYPE] = "le type est trop long";
    }

    // Si toute les valeurs sont ok on fait les traitement suivant
    if ($valeursOK && ctype_alnum($_POST[CONTACT])) {
        //On appel la methode qui insert un tel en base
        modifNumTel($_GET["numero"], $_POST[NUMERO], $_POST[CONTACT], $indicatif, $_POST[TYPE]);
        //On redirige l'utisateur vers la page id du contact
        header("Location: pagePerso?ID=" . $_POST[CONTACT]);
    }
}
else{
    $_POST[NUMERO] = $info["numero"];
    $_POST[INDICATIF] = $info["indicatif"];
    $_POST[TYPE] = $info["typeNumero"];
    $_POST[CONTACT] = $info["contact"];
    if (isset($_GET['ID'])) {
        $_POST[CONTACT] = $_GET['ID'];
    }
    else{
        $_GET['ID'] = -1;
    }
}
?>

<!DOCTYPE html>

<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../styles/style.css" rel="stylesheet" type="text/css" media="all" />
    <title>Ajout Téléphone</title>
</head>

<body class='content'>
    <?php
    //On créer l'objet qui construit le formulaire avec la methode, l'action et le nom du formulaire.
    $formAddNumTel = new FormBuilder("POST", "numeroModification"."?numero=".$_GET['numero'], "Modification d'un numero");

    //On rajouter les champs avec leurs noms leurs type et les message d'erreur potentiel.
    $formAddNumTel->addChamp(NUMERO, NUMERO, "text", $erreurs[NUMERO]);
    $formAddNumTel->addSelect(CONTACT,arrayOfContact());
    $formAddNumTel->addSelect(INDICATIF,arrayOfIndicatif());
    $formAddNumTel->addChamp(TYPE, TYPE, "text", $erreurs[TYPE]);
    $formAddNumTel->addSubmit("envoyer");
    $formAddNumTel->addHiddenSubmit();
    ?>
    <a href="../index.php"><input type="button" value="Retour"></a>
    <?php
    $formAddNumTel->finishForm();
    ?>
</body>
<?php require_once('layout/footer.php') ?>

</html>