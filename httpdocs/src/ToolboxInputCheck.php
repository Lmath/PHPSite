<?php

class ToolboxInputCheck
{

    /**
     * Test la présence d'un nombre ou character special dans un string
     * @param valeur = le string a test
     * @return bool
     */
    private function hasNumberOrSpecialChar($valeur)
    {
        return 0 != preg_match('/[^A-Za-z\'éèàîïâäêëûü]/', $valeur);
    }

    /**
     * Test qu'un string ne contient que des nombres
     * @param valeur = le string a test
     * @return bool
     */
    private function hasJustNumber($valeur)
    {
        return ctype_digit($valeur);
    }

    /**
     * Test qu'un indicatif est bien formé
     * @param valeur = le string a test
     * @return string
     */
    private function indicatifWellFormed($valeur, &$OK)
    {
        if (0 == preg_match('/[+][0-9]{1,3}$/', $valeur)) {
            $OK = false;
            return "L'indicatif est mal formé";
        } else {
            return "";
        }
    }

    /**
     * Renvoie un msg d'erreur si un indicatif est mal formé
     * @param valeur = le string a test
     * @param OK = pointeur sur boolean pour test si les champs son bien remplis
     * @return string
     */
    public function stringHaveWellFormedIndicatif($valeur, &$OK)
    {
        $isMandatory = $this->isMandatory($valeur, $OK);
        if (!null == $isMandatory) {
            return $isMandatory;
        } else {
            return $this->indicatifWellFormed($valeur, $OK);
        }
    }

    /**
     * Renvoie un msg d'erreur si un string n'est pas remplis
     * @param valeur = le string a test
     * @param OK = pointeur sur boolean pour test si les champs son bien remplis
     * @return string
     */
    private function isMandatory($valeur, &$OK)
    {
        if ("" == $valeur) {
            $OK = false;
            return "Ce champ est obligatoire";
        } else {
            return "";
        }
    }

    /**
     * Renvoie un msg d'erreur si un string contient autre chose que des alpha
     * @param valeur = le string a test
     * @param OK = pointeur sur boolean pour test si les champs son bien remplis
     * @return string
     */
    private function hasOnlyAlpha($valeur, &$OK)
    {
        if ($this->hasNumberOrSpecialChar($valeur)) {
            $OK = false;
            return "Ce champ ne peut contenir que des lettres";
        } else {
            return "";
        }
    }

    /**
     * Renvoie un msg d'erreur si un string contient autre chose qu'un nombre
     * @param valeur = le string a test
     * @param OK = pointeur sur boolean pour test si les champs son bien remplis
     * @return string
     */
    public function hasOnlyNumber($valeur, &$OK)
    {
        if (!$this->hasJustNumber($valeur)) {
            $OK = false;
            return "Ce champ ne peut contenir que des nombres";
        }
        return "";
    }

    /**
     * Renvoie un msg d'erreur si un string contient autre chose que des alpha ou est non remplis
     * @param valeur = le string a test
     * @param OK = pointeur sur boolean pour test si les champs son bien remplis
     * @return string
     */
    public function onlyAlphaAndMandatory($valeur, &$OK)
    {
        $isMandatory = $this->isMandatory($valeur, $OK);
        if (!null == $isMandatory) {
            return $isMandatory;
        } else {
            return $this->hasOnlyAlpha($valeur, $OK);
        }
    }

    /**
     * Renvoie un msg d'erreur si un string contient autre chose qu'un nombre ou est nom remplis
     * @param valeur = le string a test
     * @param OK = pointeur sur boolean pour test si les champs son bien remplis
     * @return string
     */
    public function onlyNumericAndMandatory($valeur, &$OK)
    {
        $isMandatory = $this->isMandatory($valeur, $OK);
        if (!null == $isMandatory) {
            return $isMandatory;
        } else {
            return $this->hasOnlyNumber($valeur, $OK);
        }
    }

    /**
     * Test la taille d'un string
     * @param str = le string a test
     * @param OK = pointeur sur boolean pour test si les champs son bien remplis
     * @param tailleMax = le nombre max de char d'un string
     * @return bool
     */
    public function strLenghtToLong($str, &$OK, $tailleMax)
    {
        if ($tailleMax < strlen($str)) {
            $OK = false;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Renvoie un msg d'erreur si un string d'une date n'est pas dans le passé ou est nom remplis
     * @param str = la date à tester en string
     * @param OK = pointeur sur boolean pour test si les champs son bien remplis
     * @return string
     * @throws Exception
     */
    public function dateIsInPastAndMandatory($valeur, &$OK)
    {
        $isMandatory = $this->isMandatory($valeur, $OK);
        if (!null == $isMandatory) {
            return $isMandatory;
        } else {
            return $this->dateIsInPast($valeur, $OK);
        }
    }

    /**
     * Renvoie un msg d'erreur si un string d'une date n'est pas dans le passé
     * @param str = la date à tester en string
     * @param OK = pointeur sur boolean pour test si les champs son bien remplis
     * @return string
     * @throws Exception ParseError
     */
    public function dateIsInPast($valeur, &$OK)
    {
        $format = 'Y-m-d';
        $dateFromUser = DateTime::createFromFormat($format, $valeur);
        $dateNow = new DateTime('NOW');
        //Set la meme horaire pour les deux dates afin d'avoir une comparaison correcte
        $dateNowWithFormat = DateTime::createFromFormat($format, $dateNow->format($format));
        if ($dateFromUser >= $dateNowWithFormat) {
            $OK = false;
            return "La date est invalide";
        } else {
            return "";
        }
    }

}