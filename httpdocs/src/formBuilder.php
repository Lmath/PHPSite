<?php

/**
 * Class permet la creation d'un formulaire et l'ajout de champs
 */
class FormBuilder
{

    /**
     * Constructeur du formulaire
     * @param method = method utilisé par le formulaire
     * @param action = lien de l'envoie du formulaire
     * @param nomFormulaire = nom renseigner dans le champs legend du formulaire
     */
    public function __construct($method, $action, $nomFormulaire)
    {
        //On "initialise" le formulaire
        $this->initaliseForm($method, $action, $nomFormulaire);
    }

    /**
     * Créé le debut du formulaire
     * @param method = method utilisé par le formulaire
     * @param action = lien de l'envoie du formulaire
     * @param nomFormulaire = nom renseigner dans le champs legend du formulaire
     */
    private function initaliseForm($method, $action, $nomFormulaire)
    {
        //On utilise htmlspecialchars pour evite l'insertion de character non html afin de limiter les fails
        echo '<form class=\'form\' method="' . htmlspecialchars($method) . '" action="' . htmlspecialchars($action) . '">';
        echo '<fieldset>';
        echo '<legend> ' . htmlspecialchars($nomFormulaire) . ' </legend>';
    }

    /**
     * Fonction publique permettant l'ajout d'un champ dans le formulaire
     * @param nomChamp = id du champs ainsi que le nom de la valeur dans le tableau $_POST pour la props value
     * @param legen = legend du champ
     * @param type = type du champ a ajouter (text/password/hidden etc...)
     * @param erreur = message d'erreur a afficher
     */
    public function addChamp($nomChamp, $legend, $type, $erreur)
    {
        ?>
        <div>
            <label for="<?= $nomChamp ?>" class="inputLabel"><?= $legend ?>:
                <!-- a la premier initatialisation la valeur $_POST[$nomChamp] est null car pas encore initialiser si cette derniere existe ou pas avant de l'afficher -->
                <input class="inputBasic" name="<?= $nomChamp ?>" type="<?= $type ?>"
                       value="<?= $this->firstInitialisation($nomChamp) ?>"/>
            </label>
            <?php
            //Si il y a un message d'erreur on l'affiche
            if ('' != $erreur) {
                echo "<span class='errorText'>$erreur</span>"; //on rajoute à la fonction un quatrième paramètre qui contient le message d'erreur
            }
            ?>
        </div>
        <?php
    }

    /**
     * Ajouter la bouton de submit pour envoyer le formulaire
     * @param nomChamp = id du champ submit ainsi que sa valeur
     */
    public function addSubmit($nomChamp)
    {
        ?>
        <input class="submitButton" name="<?= $nomChamp ?>" type="submit" value="<?= $nomChamp ?>"/>
        <?php
    }

    /**
     * Ajouter un select avec un tableau d'options
     * @param nomChamp = nom du champ
     * @param options = liste des options a afficher dans select
     */
    public function addSelect($nomChamp, $options)
    {
        ?>
        <label for="<?= $nomChamp ?>"><?= $nomChamp ?> :
            <select required class="inputBasic" name="<?= $nomChamp ?>">
                <?php
                //On rajoute une option pour tous les element du tableau
                foreach ($options as $option) {
                    //Pour garder la réponse "active"
                    // Si $POST est egale a une des option on la met en selected de base.
                    if ("$_POST[$nomChamp] " == preg_split("/[:]/", $option)[0]) {
                        ?>
                        <option value=<?= $option ?> selected><?= $option ?></option>
                        <?php
                    } else {
                        ?>
                        <option value=<?= $option ?>><?= $option ?></option>
                        <?php
                    }
                }
                ?>
            </select>
        </label>
        <br/>
        <?php
    }

    /**
     * Ajoute un input vide et caché pour verif qu'il y a deja eu un click sur submit afin de check les valeurs.
     */
    public function addHiddenSubmit()
    {
        ?>
        <input name="soumission" type="hidden"/>
        <?php
    }

    /**
     * Vérifie sur la valeur $_POST[$nomChamp] est nul ou non et renvoie soit sa valeur ou sinon une chaine vide
     * @param nomChamp = nom du champ dans la tableau POST a test
     * @return string
     */
    private function firstInitialisation($nomChamp)
    {
        if (empty($_POST[$nomChamp])) {
            return "";
        } else {
            $nom = $_POST[$nomChamp];
            $safeName = htmlspecialchars($nom);
            return "$safeName";
        }
    }

    /**
     * Fermeture de formulaire
     */
    public function finishForm()
    {
        echo '</fieldset></form>';
    }
}

?>