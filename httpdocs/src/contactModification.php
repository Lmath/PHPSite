<?php

/**
 * Formulaire de modification d'un contact
 */
include 'formBuilder.php';
include 'ToolboxInputCheck.php';
include '../configPDO/utilsPDO.php';
$info = contactInfo($_GET['ID']);


//Déclaration des constante de taille et nom des champs
const TAILLENOM = 50;
const TAILLEPRENOM = 30;
const TAILLESTR = 256;
const TAILLECODEPOSTAL = 5;
const PRENOM = "prenom";
const NOM = "nom";
const SOCIETE = "societe";
const ADRESSE = "adresse";
const DATE_NAISSANCE = "DATE_NAISSANCE";
CONST CODE_POSTAL = "code_postal";


//Preparation des messages d'erreurs
$erreurs[NOM] = "";
$erreurs[PRENOM] = "";
$erreurs[SOCIETE] = "";
$erreurs[ADRESSE] = "";
$erreurs[DATE_NAISSANCE] = "";
$erreurs[CODE_POSTAL] = "";

//Création du formatter pour les différents inputs
$formatter = new ToolboxInputCheck;

//Check des valeurs des champs apres click sur submit
if (isset($_POST["soumission"])) {
    $valeursOK = true;

    /**
     * Champ NON
     * doit etre en alpha et obligatoire
     */
    $erreurs[NOM] = $formatter->onlyAlphaAndMandatory($_POST[NOM], $valeursOK);
    //Et de taille maximum 50
    if ($formatter->strLenghtToLong($_POST[NOM], $valeursOK, TAILLENOM)) {
        $erreurs[NOM] = "le nom est trop long";
    }

    /**
     * Champ PRENON
     * doit etre en alpha et obligatoire
     */
    $erreurs[PRENOM] = $formatter->onlyAlphaAndMandatory($_POST[PRENOM], $valeursOK);
    //Et de taille maximum 30
    if ($formatter->strLenghtToLong($_POST[PRENOM], $valeursOK, TAILLEPRENOM)) {
        $erreurs[PRENOM] = "le prenom est trop long";
    }

    /**
     * Champ SOCIETE
     * de taille maximun 255
     */
    if ($formatter->strLenghtToLong($_POST[SOCIETE], $valeursOK, TAILLESTR)) {
        $erreurs[SOCIETE] = "le nom de societe est trop long";
    }

    /**
     * Champ ADRESSE
     * de taille maximun 255
     */
    if ($formatter->strLenghtToLong($_POST[ADRESSE], $valeursOK, TAILLESTR)) {
        $erreurs[ADRESSE] = "l'adresse est trop longue";
    }

    /**
     * Champ DATE_NAISSANCE
     * doit etre dans le passé et obligatoire
     */
    $erreurs[DATE_NAISSANCE] = $formatter->dateIsInPastAndMandatory($_POST[DATE_NAISSANCE], $valeursOK);

    /**
     * Champ codepostal
     * doit etre en alpha et obligatoire
     */
    if ($formatter->strLenghtToLong($_POST[CODE_POSTAL], $valeursOK, TAILLECODEPOSTAL)) {
        $erreurs[CODE_POSTAL] = "le code postal est trop long";
    }

    // Si toute les valeurs sont ok on fait les traitement suivant
    if ($valeursOK) {
        //On appel la methode qui insert un contact en base
        modifContact($_GET['ID'], $_POST[NOM], $_POST[PRENOM], $_POST[SOCIETE], $_POST[ADRESSE], $_POST[DATE_NAISSANCE], $_POST[CODE_POSTAL]);
        //On redirige l'utisateur vers le tableau des contacts
        header("Location: pagePerso.php?ID=".htmlspecialchars($_GET['ID']));
    }
}
else{
    $_POST[NOM] = $info["nom"];
    $_POST[PRENOM] = $info["prenom"];
    $_POST[SOCIETE] = $info["adresse"];
    $_POST[ADRESSE] = $info["société"];
    $_POST[DATE_NAISSANCE] = $info["DateNaissance"];
    $_POST[CODE_POSTAL] = $info["codePostal"];
}
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../styles/style.css" rel="stylesheet" type="text/css" media="all"/>
    <title>Ajout contact</title>
</head>

<body class='content'>
<?php
//On créer l'objet qui construit le formulaire avec la methode, l'action et le nom du formulaire.
$formAddContact = new FormBuilder("POST", "contactModification?ID=".htmlspecialchars($_GET['ID']), "Modification d'un contact");

//On rajouter les champs avec leurs noms leurs type et les message d'erreur potentiel.
$formAddContact->addChamp(NOM, NOM, "text", $erreurs[NOM]);
$formAddContact->addChamp(PRENOM, PRENOM, "text", $erreurs[PRENOM]);
$formAddContact->addChamp(SOCIETE, SOCIETE, "text", $erreurs[SOCIETE]);
$formAddContact->addChamp(ADRESSE, ADRESSE, "text", $erreurs[ADRESSE]);
$formAddContact->addChamp(DATE_NAISSANCE, "date de naissance", "date", $erreurs[DATE_NAISSANCE]);
$formAddContact->addChamp(CODE_POSTAL, CODE_POSTAL, "text", $erreurs[CODE_POSTAL]);
$formAddContact->addSubmit("envoyer");
$formAddContact->addHiddenSubmit();
?>
<a href="../index.php"><input type="button" value="Retour"></a>
<?php
$formAddContact->finishForm();
?>
</body>

<?php require_once('layout/footer.php') ?>

</html>