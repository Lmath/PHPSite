-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mer. 12 fév. 2020 à 17:25
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `annuaire`
--

-- --------------------------------------------------------

--
-- Structure de la table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
CREATE TABLE IF NOT EXISTS `contacts` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(50) NOT NULL,
  `Prenom` varchar(30) NOT NULL,
  `Societe` varchar(256) NOT NULL,
  `Adresse_Postal` varchar(256) NOT NULL,
  `Date_Naissance` date NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `contacts`
--

INSERT INTO `contacts` (`ID`, `Nom`, `Prenom`, `Societe`, `Adresse_Postal`, `Date_Naissance`) VALUES
(1, 'Jean', 'Bombeure', '', '', '0000-00-00'),
(5, 'Arka', 'Chon', '', '', '0000-00-00'),
(6, 'Cape', 'Itemflame', '', '', '0000-00-00'),
(9, 'Peter', 'Pan', '', '', '0000-00-00'),
(12, 'Jean', 'Terre', '', '', '0000-00-00'),
(13, 'Kristopher', 'Dupont', '', '', '0000-00-00'),
(14, 'Julien', 'Martin', '', '', '0000-00-00'),
(15, 'Michelle', 'De La Montagne', '', '', '0000-00-00'),
(16, 'Richard', 'De Reydellet De Chavagnac', '', '', '0000-00-00'),
(17, 'Michelle', 'De La Montagne', '', '', '0000-00-00'),
(18, 'Richard', 'De Reydellet De Chavagnac', '', '', '0000-00-00'),
(19, 'Kim', 'Il-sung', '', '', '0000-00-00'),
(20, 'Kim', 'Jong-il', '', '', '0000-00-00'),
(21, 'Jean', 'Test', 'de La villardiere', '145 boulevard des fleurs', '2020-02-24'),
(23, 'SanChez', 'Hugo', 'Chaise', '121 rue de la chaise', '2020-02-20'),
(36, 'Toadstool', 'Peach', 'Royaume Champignon', 'ChÃ¢teau du Royaume Champignon', '0000-00-00'),
(37, 'salut', 'yo', '', '', '0000-00-00');

-- --------------------------------------------------------

--
-- Structure de la table `indicatifs_telephonique`
--

DROP TABLE IF EXISTS `indicatifs_telephonique`;
CREATE TABLE IF NOT EXISTS `indicatifs_telephonique` (
  `IdIndicatif` int(5) NOT NULL AUTO_INCREMENT,
  `NomPays` varchar(60) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `IndicatifPays` varchar(10) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL COMMENT 'Ex :  33',
  PRIMARY KEY (`IdIndicatif`)
) ENGINE=InnoDB AUTO_INCREMENT=231 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `indicatifs_telephonique`
--

INSERT INTO `indicatifs_telephonique` (`IdIndicatif`, `NomPays`, `IndicatifPays`) VALUES
(1, 'Afghanistan', '93'),
(2, 'AfriqueduSud', '27'),
(3, 'Albanie', '355'),
(5, 'Allemagne', '49'),
(6, 'Andorre', '376'),
(7, 'Angola', '244'),
(8, 'Anguilla', '12642'),
(9, 'Antigua-et-Barbuda', '12682'),
(10, 'Arabiesaoudite', '966'),
(11, 'Argentine', '54'),
(13, 'Aruba', '297'),
(15, 'Australie', '61'),
(16, 'Autriche', '43'),
(18, 'Bahamas', '12422'),
(20, 'Bangladesh', '880'),
(21, 'Barbade', '12462'),
(22, 'Belgique', '32'),
(23, 'Belize', '501'),
(25, 'Bermudes', '14412'),
(26, 'Bhoutan', '975'),
(28, 'Birmanie', '95'),
(29, 'Bolivie', '591'),
(31, 'Botswana', '267'),
(33, 'Brunei', '673'),
(34, 'Bulgarie', '359'),
(35, 'BurkinaFaso', '226'),
(36, 'Burundi', '257'),
(37, 'Cambodge', '855'),
(38, 'Cameroun', '237'),
(39, 'Canada', '12'),
(40, 'Cap-Vert', '238'),
(43, 'Chili', '56'),
(45, 'Chypre', '357'),
(46, 'Colombie', '57'),
(47, 'Comores', '269'),
(53, 'CostaRica', '506'),
(55, 'Croatie', '385'),
(56, 'Cuba', '53'),
(58, 'Danemark', '45'),
(59, 'Djibouti', '253'),
(61, 'Dominique', '17672'),
(66, 'Espagne', '34'),
(67, 'Estonie', '372'),
(71, 'Fidji', '679'),
(72, 'Finlande', '358'),
(73, 'France', '33'),
(74, 'Gabon', '241'),
(75, 'Gambie', '220'),
(77, 'Ghana', '233'),
(78, 'Gibraltar', '350'),
(80, 'Grenade', '14732'),
(81, 'Groenland', '299'),
(82, 'Guadeloupe', '590'),
(83, 'Guam', '16712'),
(84, 'Guatemala', '502'),
(88, 'Guyana', '592'),
(89, 'Guyane', '594'),
(91, 'Honduras', '504'),
(92, 'HongKong', '852'),
(93, 'Hongrie', '36'),
(94, 'Inde', '91'),
(96, 'Irak', '964'),
(97, 'Iran', '98'),
(98, 'Irlande', '353'),
(99, 'Islande', '354'),
(101, 'Italie', '39'),
(103, 'Japon', '81'),
(104, 'Jordanie', '962'),
(105, 'Kazakhstan', '7'),
(106, 'Kenya', '254'),
(107, 'Kirghizistan', '996'),
(108, 'Kiribati', '686'),
(109, 'Kosovo', '383'),
(111, 'Laos', '856'),
(112, 'Lesotho', '266'),
(113, 'Lettonie', '371'),
(114, 'Liban', '961'),
(115, 'Liberia', '231'),
(116, 'Libye', '218'),
(117, 'Liechtenstein', '423'),
(118, 'Lituanie', '370'),
(119, 'Luxembourg', '352'),
(120, 'Macao', '853'),
(122, 'Madagascar', '261'),
(123, 'Malaisie', '60'),
(124, 'Malawi', '265'),
(125, 'Maldives', '960'),
(126, 'Mali', '223'),
(127, 'Malouines', '500'),
(128, 'Malte', '356'),
(130, 'Maroc', '212'),
(132, 'Martinique', '596'),
(133, 'Maurice', '230'),
(134, 'Mauritanie', '222'),
(135, 'Mayotte', '262'),
(136, 'Mexique', '52'),
(138, 'Moldavie', '373'),
(139, 'Monaco', '377'),
(140, 'Mongolie', '976'),
(142, 'Montserrat', '16642'),
(143, 'Mozambique', '258'),
(144, 'Namibie', '264'),
(145, 'Nauru', '674'),
(147, 'Nicaragua', '505'),
(148, 'Niger', '227'),
(149, 'Nigeria', '234'),
(150, 'Niue', '683'),
(154, 'Oman', '968'),
(155, 'Ouganda', '256'),
(157, 'Pakistan', '92'),
(158, 'Palaos', '680'),
(160, 'Panama', '507'),
(162, 'Paraguay', '595'),
(163, 'Pays-Bas', '31'),
(165, 'Philippines', '63'),
(166, 'Pologne', '48'),
(168, 'PortoRico', '12'),
(169, 'Portugal', '351'),
(170, 'Qatar', '974'),
(172, 'Roumanie', '40'),
(173, 'Royaume-Uni', '44'),
(174, 'Russie', '7'),
(175, 'Rwanda', '250'),
(178, 'Sainte-Lucie', '17582'),
(179, 'Saint-Marin', '378'),
(180, 'Saint-Pierre-et-Miquelon', '508'),
(181, 'Saint-Vincent-et-les-Grenadines', '17842'),
(182, 'Salomon', '677'),
(183, 'Salvador', '503'),
(184, 'Samoa', '685'),
(188, 'Serbie', '381'),
(189, 'Seychelles', '248'),
(190, 'SierraLeone', '232'),
(191, 'Singapour', '65'),
(192, 'Slovaquie', '421'),
(194, 'Somalie', '252'),
(195, 'Soudan', '249'),
(196, 'SoudanduSud', '211'),
(197, 'SriLanka', '94'),
(199, 'Suisse', '41'),
(200, 'Suriname', '597'),
(201, 'Eswatini', '268'),
(202, 'Syrie', '963'),
(203, 'Tadjikistan', '992'),
(204, 'Tanzanie', '255'),
(206, 'Tchad', '235'),
(209, 'Timororiental', '670'),
(210, 'Togo', '228'),
(211, 'Tokelau', '690'),
(212, 'Tonga', '676'),
(214, 'Tunisie', '216'),
(217, 'Turquie', '90'),
(218, 'Tuvalu', '688'),
(219, 'Ukraine', '380'),
(220, 'Uruguay', '598'),
(221, 'Vanuatu', '678'),
(223, 'Venezuela', '58'),
(227, 'Wallis-et-Futuna', '681'),
(229, 'Zambie', '260'),
(230, 'Zimbabwe', '263');

-- --------------------------------------------------------

--
-- Structure de la table `num_tel`
--

DROP TABLE IF EXISTS `num_tel`;
CREATE TABLE IF NOT EXISTS `num_tel` (
  `Numero` varchar(10) NOT NULL,
  `Id_Contact` int(11) NOT NULL,
  `TypeNum` varchar(50) NOT NULL,
  `Id_Indicatif` int(4) NOT NULL,
  PRIMARY KEY (`Numero`),
  KEY `Id_Contact` (`Id_Contact`),
  KEY `foreignKeyIndicatif` (`Id_Indicatif`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `num_tel`
--

INSERT INTO `num_tel` (`Numero`, `Id_Contact`, `TypeNum`, `Id_Indicatif`) VALUES
('0606060606', 6, 'fixe', 43);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `num_tel`
--
ALTER TABLE `num_tel`
  ADD CONSTRAINT `foreignKey` FOREIGN KEY (`Id_Contact`) REFERENCES `contacts` (`ID`),
  ADD CONSTRAINT `foreignKeyIndicatif` FOREIGN KEY (`Id_Indicatif`) REFERENCES `indicatifs_telephonique` (`IdIndicatif`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
